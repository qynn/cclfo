/***************************************************************
 * project : ccLFO
 * target  : ATtiny841 (16MHz)
 * author  : .qynn (https://gitlab.com/qynn)
 * license : GNU GPLv3
 *
 *  Functions to generate/update LFO waves (Sine, Sinetooth, Square)
 *  using Timer0 in CTC mode @1kHz and a 8-bit lookup table
 *
 ****************************************************************/

#include "waves.h"


#define TABLE_SIZE 100

const uint8_t sinTable[TABLE_SIZE]={
127,134,142,150,158,166,173,181,188,195,
201,207,213,219,224,229,234,238,241,245,
247,250,251,252,253,254,253,252,251,250,
247,245,241,238,234,229,224,219,213,207,
201,195,188,181,173,166,158,150,142,134,
127,119,111,103,95,87,80,72,65,58,
52,46,40,34,29,24,19,15,12,8,
6,3,2,1,0,0,0,1,2,3,
6,8,12,15,19,24,29,34,40,46,
52,58,65,72,80,87,95,103,111,119};



volatile uint16_t	t_MPX 	= 0;		// timer0 index @1kHz
volatile uint16_t	t_WAV	= 0;		// timer0 index B @1kHz

uint8_t clip7(uint16_t val, uint16_t ref){ //clip value between 0 an 127 (7-bit)
	uint8_t out=0;
	if (val>(127+ref))out=127;
	else if (val<ref) out=0;
	else out=val-ref;
	return out;
}

uint8_t SineWave(uint8_t amp, uint8_t off, uint8_t mode, uint8_t i){  //get values from sineTable
    //amp: (0--127)/2 = (0--255)/4  : >>2
    //sinTable: (0--255)/1023 : >>7
    //FULL SINE : >>9 (m=0)
    //SINETOOTH : >>8 (m=1)
    uint16_t var=((uint16_t)(amp*sinTable[i]))>>(9-mode);
    uint16_t val=off+var;
    uint16_t ref=(amp>>(2-mode)); //Sine (0--64) - Tooth (0-127)
    uint8_t value=clip7(val,ref);
    return value;
}

uint8_t SquareWave(uint8_t amp, uint8_t off, uint8_t i){

      uint16_t val=0;
      if (off<63) //LOW OFFSET  (0--(off+amp/4))
      {
            if (i<=50) val=off+(amp>>2);  				// 50% duty cycle
            else val=0;
      }
      else //HIGH OFFSET (127--(off-amp/4))
      {
          if (i<=50) val=127; // 50% duty cycle
          else val=off-(amp>>2);
       }
       uint8_t value=clip7(val,0);
       return value;
}

void init_timer0(void){ /* Set up TIMER0 (8-bit) at fHz=1kHz (CTC mode) */

	/* re-initialize timer0 */
	cli(); 					// disable global interrupts
	TCCR0A = 0;				// init Timer/Control register A (Mode select)
	TCCR0B = 0;				// init Timer/Control register B (clock select)
	//TCNT0  = 0;			    // initialize counter value to 0

	/* set compare Register */
	OCR0A = 249;
	/**NB**
	 * F_CPU = 16MHz ; fHz=1kHz ; Pre = 64
	 * OCR0A = [F_CPU/(fHz*Pre) -1] = [16*10^6/(10^3*64) -1] = 249
	 * Compare Register (8-bit) MUST BE < 256 !!!
	 */

	/* set prescaler */
	TCCR0A |= (1 << COM0A1) | (1 << WGM01);  	   // clear OC0A on Compare Match in CTC mode
    TCCR0B |= (1 << CS00) | (1 << CS01);   		   // 64 prescaler

	TIMSK0 |= (1 << OCIE0A); // enable Output Compare Match A Intterupt on TIMER0 (COMPA)

	sei();	//enable global interrupts
}

ISR(TIMER0_COMPA_vect){ // WARNING : <core_timers.h> redefines TIMn_COMPm_vect into TIMERn_COMPm_vect !!!!
	t_MPX++;
	t_WAV++;
}
