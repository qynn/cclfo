/**********************************************************
* project : ccLFO
* target  : ATtiny841
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
************************************************************/


#ifndef MIDIOUT_H
#define MIDIOUT_H

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <avr/io.h>

//Default Midi Status associated w/ Channel 1
#define NOTE_ON		144
#define NOTE_OFF	128
#define CTRL_CG		176

/*replacement of loop_until_bit_is_set()*/
#define wait_for_flag(port, bitnum) \
while ( ! ( (port) & (1 << (bitnum)) ) ) ;

void init_midi(void);

void send_midi(uint8_t MIDIcmd, uint8_t MIDInot, uint8_t MIDIval);

uint8_t note_ON(uint8_t MIDIch, uint8_t MIDInot, uint8_t MIDIvel);

void note_OFF(uint8_t MIDIch, uint8_t MIDInot, uint8_t MIDIvel);

void send_cmd(uint8_t b);

#endif /* MIDIOUT_H */
