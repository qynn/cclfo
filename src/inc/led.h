/***********************************************************************
*  project  : ccLFO
*  target   : ATtiny841 (16MHz)
*  author   : .qynn (https://gitlab.com/qynn)
*  license  : GNU GPLv3
*
**********************************************************************/

 #ifndef LED_H
#define LED_H

#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#define LED_DDR DDRB
#define LED_PRT PORTB
#define LED_PIN 2

void init_led(void);

void init_timer1(void); // 10-bit Fast PWM

void led_on(void);

void led_off(void);

#endif /* LED_H */
