/***************************************************************
* project : ccLFO
* target  : ATtiny841 (16MHz)
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
****************************************************************/

#ifndef WAVES_H
#define WAVES_H

#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>

uint8_t clip7(uint16_t val8, uint16_t ref);

uint8_t SineWave(uint8_t amp, uint8_t off, uint8_t mode, uint8_t i);

uint8_t SquareWave(uint8_t amp, uint8_t off, uint8_t i);

void init_timer0(void); // CTC mode @10kHz


#endif // WAVES_H
