/***************************************************************
* project : ccLFO
* target  : ATtiny841 (16MHz)
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
****************************************************************/

#ifndef CCLFO_H
#define CCLFO_H

#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/io.h>
#include <math.h>

#include "led.h"
#include "pots.h"
#include "waves.h"
#include "midiOut.h"
#include "serialTx.h"

//-----------PUSH BUTTON-------------
#define SW_PIN 6
#define SW_DDR DDRA
#define SW_PRT PINA

//-----------POTENTIOMETERS-------------
const uint8_t POTS[NUM_POTS]={MOD_POT, AMP_POT, SET_POT, TMP_POT};

//-----------MIDI-------------
const uint8_t Channel[3]={2,8,4}; //Midi Channels Waldorf/Korg/MB
//[1]: Waldorf Rocket (0)  {Cutoff,Resonance,Decay,EnvMod}
//[2]: Korg Minilogue (1)  {Cutoff,Resonance,Decay,Release}
//[3]: MB 33          (2)  {Cutoff,Resonance,Decay,Attack}

//CC messages:
const uint8_t CCTable[3][4]={{74,71,75,73},{43,44,19,33},{107,106,104,103}};

// fixed offset values for last parameter (modes 9,10,11)
const uint8_t CC4off[3]={64,0,64};//[Waldorf EnvMod, Korg Release, MB Attack]

//---------Mode Selector----------
const uint8_t M_RES=5; // mode select. resolution

const uint8_t Mode[11]={
  0, 		 //mode 0  sineWave w/ off. CUTOFF
  25, 		//mode 1  sineTooth w/ off. CUTOFF
  51, 		//mode 2  sQuineWave w/ off. CUTOFF
  77, 		//mode 3  sineWave w/ off. RESONANCE
  102, 		//mode 4  sineTooth w/ off. RESONANCE
  128, 		//mode 5  squineWave w/ off. RESONANCE
  154, 		//mode 6  sineWave w/ off. DECAY
  180, 		// mode 7  sineTooth w/ off. DECAY
  206, 		//mode 8  sQuineWave w/ off. DECAY
  231, 		//mode 9  sineWave Attack (zero-based: Amp=MAX)
  255}; 	//mode 10  sineTooth Atttack (zero-based: Amp=MAX)
  //~ 1023,1023}; 	//mode 11 squineWave Attack (zero-based: Amp=MAX)

#endif /*CCLFO_H*/
