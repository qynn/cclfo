/***************************************************************
* project : ccLFO
* target  : ATtiny841 (16MHz)
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
*  Multiple Analog Readings (8-bit or 10-bit)
*  using ADC 0-11 on ATtiny841 (16MHz)
*
****************************************************************/

#include <avr/io.h>
#include <avr/interrupt.h>

#include "pots.h"


void init_pots(void){

/********NB**********
 ***PRESCALER
 *  ADC clock frequency must be between 50kHz and 200kHz
 *  ADC prescaler must be set accordingly
 *  F_CPU=16MHz ; Pre=128 >> 125kHz
 *  A conversion takes 13 ADC clock cycles (idle)
 *  13/125kHz= 104 microseconds (~ 9,6kHz)
 *
 ***RESOLUTION
 * ADLAR= 1 enables the Left-shift result presentation
 * 		ADCH |b9|b8|b7|b6|b5|b4|b3|b2|
 * 		ADCL |b1|b0|--|--|--|--|--|--|
 * This way, if no more than 8-bit precision is required
 * it is sufficient to read ADCH (ADC9 to ADC2)
 * Otherwise one must read ADCL first then ADCH
 *
 ***REF. VOLTAGE
 * By default REFS1=0; REFS2=0; in ADMUX register
 * reference voltage is then set to VCC
 *
 ***INTERRUPT
 * Setting ADIE=1 enables the ADC Conversion Complete Interrupt
 * so one can use the ADC Interrupt Servide Routine : ISR(ADC_vect){ }
 *
 */
	cli();
	ADMUXA = (1 << MUX0);
	ADCSRA = (1 << ADEN)  |  //enables ADC WHITHOUT interrupts
			 (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);   //set Prescaler to 128
    ADCSRB = (1 << ADLAR); // left-shift result for 8-bit readings
	sei();

    DDRA &= ~(1<<TMP_POT) & ~(1<<SET_POT) & ~(1<<AMP_POT) & ~(1<<MOD_POT);

}

void set_MPX(uint8_t ch){
	//~ ADCSRA &= ~(1<<ADEN); //switch off ADC

	/* ADMUXA = |-|-|MUX5|MUX4|MUX3|MUX2|MUX1|MUX0| */
	//~ ADMUXA = ( (0xF) & ch );
	ADMUXA = ch;  //ATtiny841 has 0xC=12 ADCs

	//~ ADCSRA|=(1<<ADEN); // re-enable ADC
}

uint8_t read_ADC_8bit(void){

		ADCSRA|=(1<<ADSC);  //start new conversion

		/* ADSC=1 as long as a conversion is in process */

		while (ADCSRA & (1<<ADSC));//wait until conversion is complete

	return ADCH;
}

uint16_t read_ADC_10bit(void){

		ADCSRA|=(1<<ADSC);  //start new conversion

		/* ADSC=1 as long as a conversion is in process */

		while (ADCSRA & (1<<ADSC)); //wait until conversion is complete

		/*NB: left-shifted data (ADLAR=1)
		 * ADCH |b9|b8|b7|b6|b5|b4|b3|b2|
		 * ADCL |b1|b0|--|--|--|--|--|--|
		 */

	uint16_t lsb=(uint16_t)ADCL; // ! ADCL must be read first !
	uint16_t msb=(uint16_t)ADCH;
	return ( (msb<<2)| (lsb>>6) );
}
