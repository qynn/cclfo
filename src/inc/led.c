/***********************************************************************
*  project  : ccLFO
*  target   : ATtiny841 (16MHz)
*  author   : .qynn (https://gitlab.com/qynn)
*  license  : GNU GPLv3
*
*  Timer1 is used in 8-bit Fast PWM mode for LED dimming (PB2 = TOCC7)
*
*  PWM duty cycle is adjusted trhough OCR1A (0-255)
*
**********************************************************************/

#include "led.h"

void init_timer1(void){ /* Set up TIMER1 (16-bit) in 10-bit Fast PWM (non-inverting) mode on PB2 = TOCC7 */

	/* re-initialize timer1 */
	cli(); 		// disable global interrupts
	TCCR1A = 0;	// init Timer Control register A (Mode select)
	TCCR1B = 0;	// init Timer Control register B (clock select)
	TOCPMSA1=0; // init Timer Output Compare Pin Mux register
	TOCPMCOE=0; // init Timer Output Compare Channel Output Enable
    TCNT0  = 0;	// initialize counter value to 0

	/* init compare Register */
	OCR1A = 127;
	/**NB**
	 * F_CPU = 16MHz ; Pre =256; TOP=0xFF
	 * fHz= 16MHz/256/255 = 245 Hz
	 * Compare Register (8-bit) MUST BE < 255 !
	 */

	/* set prescaler */
	TCCR1A |= (1 << COM1A1);   // clear OC1A on Compare Match (non-inverting mode)
	TCCR1A |= (1 << WGM10);    // 8-bit Fast PWM (0x00FF as TOP)
    TCCR1B |= (1 << WGM12);    // 8-bit Fast PWM (0x00FF as TOP)
    TCCR1B |= (1 << CS12);	   // 256 Prescaler
    TOCPMSA1 |= (1<< TOCC7S0); // routing OC1A to TOCC7
    TOCPMCOE |= (1<< TOCC7OE); // enable output on TOCC7

	sei();	//enable global interrupts
}

void init_led(void){
	LED_DDR |= (1<<LED_PIN);   // set PIN as output
}

void led_on(void){
	LED_PRT |= (1<<LED_PIN);   // set PIN high
}

void led_off(void){
	LED_PRT &= ~(1<<LED_PIN);  // set PIN low
}
