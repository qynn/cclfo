/***************************************************************
* project : ccLFO
* target  : ATtiny841
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
* Serial UA(R)T (TX1) for ATtiny841 (16MHz)
*
****************************************************************/
#include "serialTx.h"

//~ #ifndef F_CPU
    //~ #define F_CPU 16000000UL
//~ #endif

#define BAUD 115200
// BaudRate Expression for double speed Mode :
// UBBR = FCPU/8BAUD -1 +0.5 (rounded)
//#define UBRRV (( (F_CPU) + 4UL * (BAUD) ) / (8UL * (BAUD) ) - 1UL)

// BaudRate Expression for normal asynchronous Mode :
// UBBR = FCPU/16BAUD -1 +0.5 (rounded)
#define UBRR1V (( (F_CPU) + 8UL * (BAUD) ) / (16UL * (BAUD) ) - 1UL)
/*alternately check  #include <util/setbaud.h> */

void init_uatx1(void) {
    UBRR1H = (UBRR1V>>8);     			// UBBRV MSB
    UBRR1L = (UBRR1V& 0xff); 			// UBBRV LSB
    //UCSR0A |= (1<<U2X0); 			// using 2X speed
    UCSR1C |= (1<<UCSZ11) | (1<<UCSZ10); 	// 8N1 (sync) mode
    UCSR1B |= (1<<TXEN1);   			// enable TX1

}

void send_byte(uint8_t b) {
    wait_for_flag(UCSR1A, UDRE1); /* Wait until data register is empty. */
    UDR1 = b;
} /*alternately wait until transmission is ready: wait_for_flag(UCSR1A,TXC1)*/


void send_int(int a){
    uint8_t lgth=5;
    char buf[lgth]; //4 digits + nul character ('\0')
    char *str=&buf[lgth-1]; //now points to last character
    *str ='\0'; //nul character to terminate string
    do{
        char c = a % 10;
        *(--str)= c + '0'; //ascii digits start at '0'=48;
        a/=10;
    }while(a);

    send_str(str);
}

void send_str(char *s){
    uint8_t i= 0; //counter

    send_byte('\0'); //send dummy byte to avoid errors on first character
    while ( (s[i] != '\0') & (i < STR_MAX) ) //end of string
    {
        send_byte(s[i]);
        i++;
    }
}
