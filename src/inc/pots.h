/***************************************************************
* project : ccLFO
* target  : ATtiny841 (16MHz)
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
****************************************************************/

#ifndef POTS_H
#define POTS_H

#define NUM_POTS 4

#define AMP_POT 7  // ampli. (UNITS)  PA7 = ADC7 = MUX7
#define SET_POT 3  // offset (SET)    PA3 = ADC3 = MUX3
#define MOD_POT 2  // mode   (TENS)   PA2 = ADC2 = MUX2
#define TMP_POT 0  // tempo  (TIME)   PA0 = ADC0 = MUX0

void init_pots(void); //init ADC: single-ended input, Vref=VCC, left-shifted result

void set_MPX(uint8_t ch); //select ADC channel (1, 2 or 3)

uint8_t read_ADC_8bit(void); // start conversion and read MSB

uint16_t read_ADC_10bit(void); //start conversion and read (MSB + LSB)

#endif /* POTS_H */
