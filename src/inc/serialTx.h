/***************************************************************
* project : ccLFO
* target  : ATtiny841 (16MHz)
* author  : .qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
****************************************************************/

#ifndef SERIALTX_H
#define SERIALTX_H

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <avr/io.h>

#define STR_MAX	20   // Maximum length for Strings

/*replacement of loop_until_bit_is_set()*/
#define wait_for_flag(port, bitnum) \
while ( ! ( (port) & (1 << (bitnum)) ) ) ;

void init_uatx1(void);

void send_byte(uint8_t b);

void send_int(int a);

void send_str(char *s);

#endif /* SERIALTX_H */
