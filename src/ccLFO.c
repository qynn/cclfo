/***************************************************************
* project : ccLFO
* target  : ATtiny841 (16MHz)
* author  : qynn (https://gitlab.com/qynn)
* license : GNU GPLv3
*
* Minimal MIDI (CC) LFO based on ATtiny841 (16MHz)
*
* New Low Fuse Byte : 1100 1110 = 0x CE
****************************************************************/

#include "inc/ccLFO.h"


#define DEBUG 1
/**********INIT***********/

//-----Timing-------
uint8_t i=0,i0=0,iN=100;// LFO index/start/end
extern volatile uint16_t t_MPX; // elapsed time (ms) (TIMER1@1kHz)
extern volatile uint16_t t_WAV; // elapsed time (ms) (TIMER1@1kHz)
const uint16_t T_MPX=100;  //Multiplexer timeout (ms)
//-----Panel-------
uint8_t dt=0; // time delay from tempo Pot. (A1)
uint8_t dpth=0; //wave depth (0-255) from Amp Pot. (A3)
uint8_t m=11; //mode index 0-11 (see Mode[] in ccLFO.h)
uint8_t c=0; //machine index 0-2 (see Channel[] in ccLFO.h)
//-----MIDI-------
uint8_t CCstatus=176; // Control Change status number
uint8_t CCnumber=74; //CC number
uint8_t CCvalue=127, CCoffset=56;//  CCvalue, CCoffset
//-----Multiplexer------
uint8_t i_MPX=0; //Multiplexer Channel Index


//-----------SWITCH -------------
void initSwitch(void){ //init. ON/OFF push button on PA6
	SW_DDR &= ~(1<<SW_PIN); //set Switch Pin as input
}

void checkSwitch(void){  //  0V/5V on swPin (PB0)
	if ( !(SW_PRT & (1<<SW_PIN)) )
	{
		i=i0;
		t_WAV=0; //reset on button push
	}
}


void getCCnumber(uint8_t mode){ // get current Control Change number
	uint8_t r= (uint8_t)mode/3.0; //CC number index
	CCnumber=CCTable[c][r];
}

void setIndex(uint8_t mode){ //set index limits (i0:start iN:end)
	if ((mode%3)==1) //SineTooth (half-wave)
	{
		i0=75;
		iN=25;
	}
	else  //Sine or Square
	{
		i0=0;
		iN=100;

    }
    i=i0; //reset counter;
}

uint8_t getTempo(uint8_t a){ //get time delay dt in ms (5ms-160ms)
	//uint8_t dt= (uint8_t)( 160-floor(a*(155.0/1023.0)) );
	uint8_t dt= (uint8_t)( 160-floor(a*(155.0/255.0)) ); //8-bit reading
	return dt;
}

uint8_t getAmp(uint8_t a){ //get wave amplitude (0-255)
  // WARNING: Amp pot inverted : potHigh=0 ; potLow=255
    //depth=(uint8_t)(255-floor(255.0*a/1013.0)); //potHigh:1000 , potLow:0
	uint8_t amp=255-a; //(0-255) 8-bit reading
	return amp;
}

uint8_t getMode(uint8_t a, uint8_t m0){ //get mode from 8-bit ADC reading
	uint8_t m1=m0; //last mode
	if (a==0){m1=0;}
	if (a==255){m1=10;}
    else if ( !( ((Mode[m0]-M_RES)<=a) & (Mode[m0]+M_RES>a) )) //MODE HAS CHANGED
    {
		for (int k=1; k<10; k++)
		{
			if ( ((Mode[k]-M_RES)<=a) ) m1=k;
            //~ break;
		}//mode m1 selected
		getCCnumber(m1); //update CC number
		setIndex(m1); //update i0 and iN
    }//MODE HAS CHANGED
    return m1;
}

uint8_t getOffset(uint8_t a){ //get wave amplitude (0-255)
	uint8_t off=a>>1; //7-bit (0-127)
    return off;
}

void checkPot(uint8_t ch){
    set_MPX(ch); //set multiplexer to channel ch (1,2 or 3)
	switch(ch){
		case TMP_POT: //tempo
			read_ADC_8bit(); //dummy read
			dt=getTempo(read_ADC_8bit()); //(5-160)ms
			break;
		case SET_POT: //offset
            read_ADC_8bit();
			CCoffset=getOffset(read_ADC_8bit()); //(0-127)
			break;
        case MOD_POT: //mode select
            read_ADC_8bit();
            m=getMode(read_ADC_8bit(),m); //(0-10)
            break;
		case AMP_POT: // amplitude
			read_ADC_8bit(); //dummy read
			dpth=getAmp(read_ADC_8bit()); //(0-255)
			break;
	}
}

void checkChannel(void){ //only at startup
checkPot(MOD_POT);  // checkMode for midi channel selection
c=m%3;  //select channel (see ccLFO.h)
uint8_t r= (uint8_t)m/3.0; //CCnumber index
CCnumber=CCTable[c][r]; //CCnumber
CCstatus=CTRL_CG+Channel[c]-1; //CC status
}


void incr(){ //increment Table index
	uint8_t sel=m%3;
	if (sel==2)//SQUARE
	{
		  CCvalue= SquareWave(dpth,CCoffset,i);
		  i=i+2;
	}
	else //SINE (m=0) or TOOTH (m=1)
	{
		CCvalue = SineWave(dpth,CCoffset,sel,i);
		i=i+1-2*sel; //Sine : +1 / Tooth : -1;

	}
	if(i==iN) i=i0;//rollover
}

int main(){

    /*inputs*/
    init_pots();    //pots.c
    initSwitch();
    /*outputs*/
	init_led();     //led.c
	init_midi();    //midiOut.c
	init_uatx1();    //serialTx.c
	 /*timers*/
    init_timer0();  //waves.c
	init_timer1();  //led.c

	checkChannel();
	while(1)
	{

		if( (t_WAV>dt) & (dpth>0) )	 //Table increment
		{
			incr();
			send_midi(CCstatus, CCnumber, CCvalue);  // SEND CC Message
            OCR1A = 2*CCvalue;  // 8-bit PWM LED dimming
			t_WAV=0;
		}

		if( (t_MPX>T_MPX))  // Multiplexer
		{
			checkPot(POTS[i_MPX]);
			if (++i_MPX==NUM_POTS) i_MPX=0; // increment MPX channel

			#if DEBUG
				send_str(" i_");
				send_int(i);
				send_str(" T_");
				send_int(dt);
				send_str(" A_");
				send_int(dpth);
				send_str(" M_");
				send_int(m);
				send_str(" C_");
				send_int(CCnumber);
				send_str(" V_");
				send_int(CCvalue);
				send_str(" O_");
				send_int(CCoffset);
				send_str(" \r");
			#endif

			t_MPX=0;
		}
		checkSwitch();
	}//while(1)

}//main()
