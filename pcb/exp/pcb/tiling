#!/bin/bash

# Copyright [2018] [.qynn (https://gitlab.com/qynn)]
#
# 'tiling' is a simple script that generates a mosaic layout of a single image
# and is particularly helpful to print multiple PCBs at the desired size 
#
# Licensed under the Apache License, Version 2.0;
# http://www.apache.org/licenses/LICENSE-2.0

# USER DEFINED
PRO=ccLFO841    #Source file
SRC="$PRO-brd.svg"
brd_width=42  	#actual board width (mm)
brd_height=38 	#actual board height (mm)
dpi=300		    #pdf resolution (dpi)
zoom=10135		#printer compensation (10000=100,00%)

#temp. filenames
b=$(basename $SRC)
fname="${b%.*}"
source="$PRO-src.gif"
output="$PRO-out.gif"

# board size (pixels)
Wb=$(expr $brd_width \* 10 \* $dpi / 254 \* $zoom / 10000)
Hb=$(expr $brd_height \* 10 \* $dpi / 254 \* $zoom / 10000)

echo "board size (mm): $brd_width mm x $brd_height"
echo "resolution (dpi): $dpi"
echo "tile size (px): $Wb x $Hb"

#Converting source file (.svg) to (.gif) at given resolution
#convert -units PixelsPerInch -density "$dpi" $SRC -trim +repage $source
convert $SRC  -trim +repage -resize "$Wb x $Hb !" $source
#identify -format "%w x %h" $source
#echo "(.svg to .gif)"

#page setting (letter format : 8.5x11.5)
Wp=$(expr $dpi \* 17 / 2)  #paper with (px)
Hp=$(expr $dpi \* 11)      #paper height (px)
mrg=$(expr $dpi \* 1)      #global margins (px)
Wm=$(expr $Wp - $mrg \* 2) #printable width
Hm=$(expr $Hp - $mrg \* 2) #printable height
echo "page size (px): $Wp x $Hp"

#Max boards per page (default)
Nw1=$(expr $Wm / $Wb)
Nh1=$(expr $Hm / $Hb)
Nwh1=$(expr $Nw1 \* $Nh1)
#echo "Max. grid layout: $Nw1 x $Nh1 = $Nwh1"

#if rotated from 90deg:
Nw2=$(expr $Wm / $Hb)
Nh2=$(expr $Hm / $Wb)
Nwh2=$(expr $Nw2 \* $Nh2)
#echo "Alt. grid layout: $Nw2 x $Nh2 = $Nwh2"

if [ $Nwh2 -gt $Nwh1 ];
then 
	echo "Warning: rotating boards"
	convert $source -rotate 90 $source
	Nw=$Nw2
	Nh=$Nh2
	Nwh=$Nwh2

else
	Nw=$Nw1
	Nh=$Nh1
	Nwh=$Nwh1
fi
echo "grid layout: $Nw x $Nh = $Nwh"

#Tiling
cp $source $output
for i in $(seq 2 $Nw);
do
	convert $output $source +append $output
done
cp $output $source
for j in $(seq 2 $Nh);
do
	convert $output $source -append $output
done
#adjust to page size (add margins)
convert $output -gravity center -background white -extent "$Wp x $Hp" $output
#identify -verbose $output

convert $output -density $dpi "$PRO-til.pdf"
echo "done > > $PRO-til.pdf"

#remove temp. files
rm $source
rm $output


#~ while [[ $# -gt 0 ]]
#~ do
	#~ arg="$1"
	#~ shift
	#~ case $arg in
	#~ -r|--rotate)
	#~ shift
	#~ ;;
	#~ -v|--vertical)
	#~ V="$1"
	#~ echo "vertical x $V"
	#~ shift 
	#~ ;;
	#~ -h|--horizontal)
	#~ H="$1"
	#~ echo "horizontal x $H"
	#~ shift
	#~ ;;
	#~ *)  #unknown option
	#~ shift
	#~ ;;
#~ esac	
#~ done


