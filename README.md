# ccLFO  #

MIDI Control Change LFO based on ATtiny841

### Features ####

    - Wave selection (SineWave, SineTooth, SquareWave) 
    - 7-bit Amplitude/Offset adjustment
    - Tempo selection (500ms < T < 16 sec )
    - 8-bit Fast PWM LED dimming
    - Midi Out (31250 Baud)
    - FTDI Serial TX debug (115200 Baud)


### ATtiny841 ###

1. **General**

    http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-8495-8-bit-AVR-Microcontrollers-ATtiny441-ATtiny841_Datasheet.pdf

    https://www.tindie.com/products/DrAzzy/attiny84184-breakout-wserial-header-bare-board/

2. **Avrdude**

    ATtiny841 is not currently supported by avrdude. `.avrduderc` needs to be patched:

    https://github.com/adam-dej/avr-toolchain-patches/tree/master/avrdude-config


3. **Fuse Bits**

    ATtiny841 is shipped with Low Fuse Byte L:42

        CKDIV8 = 0    >>> divides clock by 8 (0 by default)
        CKOUT = 1
        B5 = 0        >>> W!: this line is inconsistent with Datasheet !
        SUT = 0
        CKSEL = 0010  >>> calibrated Internal 8MHz Oscillator

    **New Low Fuse Byte : 1100 1110 = 0x CE (external crystal oscillator @16MHz)**


